﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notepad_2._0
{
    class Program
    {
        static int pach = 0;
        static void Main(string[] args)
        {

            Method runner = new Method();
            Console.WriteLine("Приветствую тебя в записной книжке");
            Console.WriteLine("Нажми 1, что бы что-нибудь записать");
            Console.WriteLine("Нажми 2, что бы прочитать известную запись (Нужен ID)");
            Console.WriteLine("Нажми 3, что бы удалить существующую запись (Нужен ID)");
            Console.WriteLine("Нажми 4, что бы отредактировать существующую запись (Нужен ID)");
            Console.WriteLine("Нажми 5, что бы посмотреть все записи");
            Console.WriteLine("Нажми 0, что бы выйти из программы");
            int s = 10;
            while (s == 10)
            {
                try
                {
                    s = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {

                    Console.WriteLine("Меню управления на цифрах!");
                }
            }
            while (s != 0)
            {
                try
                {
                    if (s == 1)
                    {
                        runner.AddNote();
                        runner.Spravka();
                        s = int.Parse(Console.ReadLine());
                    }
                    else if (s == 2)
                    {
                        try
                        {
                            runner.ReadNote();
                        }
                        catch (KeyNotFoundException)
                        {
                            Console.WriteLine("Такой записи не существует");
                            Console.WriteLine("Добро пожаловать в главное меню");
                        }
                        runner.Spravka();
                        s = int.Parse(Console.ReadLine());
                    }
                    else if (s == 3)
                    {
                        try
                        {
                            runner.RemoveNote();
                        }
                        catch (KeyNotFoundException)
                        {
                            Console.WriteLine("Такой записи не существует");
                            Console.WriteLine("Добро пожаловать в главное меню");
                        }
                        runner.Spravka();
                        s = int.Parse(Console.ReadLine());
                    }
                    else if (s == 4)
                    {
                        try
                        {
                            runner.EditNote();
                        }
                        catch (KeyNotFoundException)
                        {
                            Console.WriteLine("Такой записи не существует");
                            Console.WriteLine("Добро пожаловать в главное меню");
                        }
                        runner.Spravka();
                        s = int.Parse(Console.ReadLine());
                    }

                    else if (s == 5)
                    {
                        runner.ReadAllNote();
                        runner.Spravka();
                        s = int.Parse(Console.ReadLine());
                    }
                    else
                    {
                        Console.WriteLine("Кажется кто-то не читает иструкцию, да? Внимательно смотри, что набираешь!");
                        s = int.Parse(Console.ReadLine());
                    }
                }
                catch (FormatException)
                {
                    s = 10;
                }
                Console.Clear();
                Console.WriteLine("Нажми 1, что бы что-нибудь записать");
                Console.WriteLine("Нажми 2, что бы прочитать известную запись (Нужен ID)");
                Console.WriteLine("Нажми 3, что бы отредактировать существующую запись (Нужен ID)");
                Console.WriteLine("Нажми 4, что бы удалить существующую запись (Нужен ID)");
                Console.WriteLine("Нажми 5, что бы посмотреть все записи");
                Console.WriteLine("Нажми 0, что бы выйти из программы");

            }

        }

        class Massiv
        {
            public static int i = 1;
            public Dictionary<int, string> firstname = new Dictionary<int, string>();
            public Dictionary<int, string> name = new Dictionary<int, string>();
            public Dictionary<int, string> thirdname = new Dictionary<int, string>();
            public Dictionary<int, int> tel = new Dictionary<int, int>();
            public Dictionary<int, DateTime> bithday = new Dictionary<int, DateTime>();
            public Dictionary<int, string> organiz = new Dictionary<int, string>();
            public Dictionary<int, string> dolj = new Dictionary<int, string>();
            public Dictionary<int, string> other = new Dictionary<int, string>();

        }
        public class Method
        {
            Massiv runer = new Massiv();
            public void AddNote()
            {
                Console.Clear();
                Console.WriteLine("Введите фамилию");
                runer.firstname.Add(Massiv.i, Console.ReadLine());
                Console.Clear();
                Console.WriteLine("Введите имя");
                runer.name.Add(Massiv.i, Console.ReadLine());
                Console.Clear();
                Console.WriteLine("Введите отчество");
                runer.thirdname.Add(Massiv.i, Console.ReadLine());
                Console.Clear();
                Console.WriteLine("Введите телефон");

                try
                {
                    runer.tel.Add(Massiv.i, int.Parse(Console.ReadLine()));
                }
                catch (FormatException)
                {
                    pach++;
                    Console.WriteLine("Раз такой умный, будешь вообще без телефона\n");
                    runer.tel.Add(Massiv.i, 0);
                }
                Console.WriteLine("Введите день рождения");
                try
                {
                    DateTime date = new DateTime(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
                    runer.bithday.Add(Massiv.i, date);
                }
                catch (FormatException)
                {
                    Console.WriteLine("Накосячил с датой. Бывает. Потом исправишь, а сейчас это дата создания записи\n");
                    runer.bithday.Add(Massiv.i, DateTime.Now);
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Накосячил с датой. Бывает. Потом исправишь, а сейчас это дата создания записи\n");
                    runer.bithday.Add(Massiv.i, DateTime.Now);
                }

                Console.WriteLine("Введите название организации");
                runer.organiz.Add(Massiv.i, (Console.ReadLine()));
                Console.Clear();
                Console.WriteLine("Введите должность");
                runer.dolj.Add(Massiv.i, (Console.ReadLine()));
                Console.Clear();
                Console.WriteLine("Поле для замечаний");
                runer.other.Add(Massiv.i, (Console.ReadLine()));
                Massiv.i++;

            }
            public void ReadNote()
            {
                Console.WriteLine("Введите ID того, кого ходите видеть");
                int x = int.Parse(Console.ReadLine());
                Console.WriteLine("Вот ваш запрос");
                Console.WriteLine("Фамилия:" + runer.firstname[x] + " " + "Имя:" + runer.name[x] + " " + "Отчество:" + runer.thirdname[x] + " " + "Телефон:" + runer.tel[x] + " " + "Дата рождения:" + runer.bithday[x] + " " + "Организация:" + runer.organiz[x] + " " + "Должность:" + runer.dolj[x] + " " + "Заметки:" + runer.other[x]);

            }
            public void RemoveNote()
            {
                Console.WriteLine("Введите ID записи, которую хотите удалить");
                int x = int.Parse(Console.ReadLine());
                runer.firstname.Remove(x);
                runer.name.Remove(x);
                runer.thirdname.Remove(x);
                runer.tel.Remove(x);
                runer.bithday.Remove(x);
                runer.organiz.Remove(x);
                runer.dolj.Remove(x);
                runer.other.Remove(x);
            }
            public void EditNote()
            {


                Console.WriteLine("Какую запись хотите отредактировать");
                int x = int.Parse(Console.ReadLine());
                Console.WriteLine("Какую часть записи хотите отредактировать");
                Console.WriteLine("1 - Фамилия");
                Console.WriteLine("2 - Имя");
                Console.WriteLine("3 - Отчество");
                Console.WriteLine("4 - Телефон");
                Console.WriteLine("5 - Дата рождения");
                Console.WriteLine("6 - Организация");
                Console.WriteLine("7 - Должность");
                Console.WriteLine("8 - Заметки");
                int y = int.Parse(Console.ReadLine());
                if (y == 1)
                {
                    Console.WriteLine("Введите новую фамилию");
                    runer.firstname[x] = Console.ReadLine();
                }
                else if (y == 2)
                {
                    Console.WriteLine("Введите новое имя");
                    runer.name[x] = Console.ReadLine();
                }
                else if (y == 3)
                {
                    Console.WriteLine("Ввведите новое отчество");
                    runer.thirdname[x] = Console.ReadLine();

                }
                else if (y == 4)
                {
                    Console.WriteLine("Введите новый телефон");
                    try
                    {
                        runer.tel[x] = int.Parse(Console.ReadLine());
                    }
                    catch (FormatException)
                    {
                        if (pach == 0)
                        {
                            pach++;
                            Console.WriteLine("Раз такой умный, будешь вообще без телефона");
                        }
                        else
                        {
                            Console.WriteLine("Опять за своё?! А вот фигушки! Не видать тебе телефона!");
                        }

                    }

                }

                else if (y == 5)
                {
                    Console.WriteLine("Введите новый день рождения");
                    try
                    {
                        DateTime date = new DateTime(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
                        runer.bithday[x] = date;
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Накосячил с датой. Бывает. Потом исправишь");
                    }
                    catch (ArgumentOutOfRangeException)
                    {

                        Console.WriteLine("Накосячил с датой. Бывает. Потом исправишь");
                    }
                }
                else if (y == 6)
                {
                    Console.WriteLine("Введите новое название организации");
                    runer.organiz[x] = Console.ReadLine();
                }
                else if (y == 7)
                {
                    Console.WriteLine("Введите новую должность");
                    runer.dolj[x] = Console.ReadLine();
                }
                else if (y == 8)
                {
                    Console.WriteLine("Обновите поле для замечаний");
                    runer.other[x] = Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Внимательнее инструкцию читай!");
                }



            }
            public void ReadAllNote()
            {
                for (int i = 1; i < Massiv.i; i++)
                {
                    try
                    {
                        Console.WriteLine("ID:" + i + "Имя:" + runer.name[i]);
                    }
                    catch (KeyNotFoundException)
                    {
                        Console.WriteLine("Такой записи не существует");
                    }
                }

            }
            public void Spravka()
            {
                Console.WriteLine("А теперь переход. Куда попасть желаете?");
                Console.WriteLine("Нажми 1, что бы что-нибудь записать");
                Console.WriteLine("Нажми 2, что бы прочитать известную запись (Нужен ID)");
                Console.WriteLine("Нажми 3, что бы отредактировать существующую запись (Нужен ID)");
                Console.WriteLine("Нажми 4, что бы удалить существующую запись (Нужен ID)");
                Console.WriteLine("Нажми 5, что бы посмотреть все записи");
                Console.WriteLine("Нажми 0, что бы выйти из программы");
            }
        }
    }
}
